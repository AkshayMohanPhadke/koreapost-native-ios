//
//  WelcomeScreenConfigurator.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import Foundation
import UIKit

class WelcomeScreenConfigurator {
    
    init() {
        
    }
}

extension WelcomeScreenConfigurator: WelcomeScreenConfiguratorInterface {
    func getWelcomeScreenViewController() -> UIViewController? {
        
        let welcomeScreenInteractor = WelcomeScreenInteractor()
        let welcomeScreenPresenter = WelcomeScreenPresenter()
        let router = WelcomeScreenRouter()
        
        welcomeScreenInteractor.presenter = welcomeScreenPresenter
        
        let welcomeScreenViewController = WelcomeScreenViewController(interactor: welcomeScreenInteractor)
        
        welcomeScreenPresenter.router = router
        welcomeScreenPresenter.viewController = welcomeScreenViewController
        
        router.viewController = welcomeScreenViewController
        
        return welcomeScreenViewController
    }
}
