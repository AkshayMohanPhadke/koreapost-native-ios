//
//  WelcomeScreenPresenter.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import Foundation

class WelcomeScreenPresenter {
    var router: WelcomeScreenRouterInterface?
    var viewController: WelcomeScreenViewControllerInterface?
    
    init() {
        
    }
}

//MARK:- WelcomeScreenPresenterInterface
extension WelcomeScreenPresenter: WelcomeScreenPresenterInterface {
    
}
