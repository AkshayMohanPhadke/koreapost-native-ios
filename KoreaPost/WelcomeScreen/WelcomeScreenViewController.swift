//
//  WelcomeScreenViewController.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import UIKit

class WelcomeScreenViewController: UIViewController {

    enum Constants {
        static var ctaButtonLeadingAnchorConstant: CGFloat = 24.0
        static var ctaButtonTrailingAnchorConstant: CGFloat = -24.0
        static var ctaButtonBottomAnchorConstant: CGFloat = -40.0
    }
    
    var interactor: WelcomeScreenInteractorInterface?
    
    init(interactor: WelcomeScreenInteractorInterface?) {
        self.interactor = interactor
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}

//MARK:- WelcomeScreenViewControllerInterface
extension WelcomeScreenViewController: WelcomeScreenViewControllerInterface {
    func setupViews() {
        view.backgroundColor = .themeScreenBackgroundColor
        
        let ctaButton = UIButton.getThemeCTAButton(title: "건너뛰기")
        view.addSubview(ctaButton)
        
        ctaButton.heightAnchor.constraint(equalToConstant: UIButton.themeCTAButtonHeightConstant).isActive = true
        ctaButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: Constants.ctaButtonLeadingAnchorConstant).isActive = true
        ctaButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: Constants.ctaButtonTrailingAnchorConstant).isActive = true
        ctaButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: Constants.ctaButtonBottomAnchorConstant).isActive = true
        
    }
}
