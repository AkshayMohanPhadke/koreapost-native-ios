//
//  WelcomeScreenInterfaces.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import Foundation
import UIKit

protocol WelcomeScreenInteractorInterface {
    
}

protocol WelcomeScreenPresenterInterface {
    
}

protocol WelcomeScreenRouterInterface {
    
}

protocol WelcomeScreenViewControllerInterface {
    func setupViews()
}

protocol WelcomeScreenConfiguratorInterface {
    func getWelcomeScreenViewController() -> UIViewController?
}
