//
//  UIButton+Exxtension.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import Foundation
import UIKit

extension UIButton {
    
    static var themeCTAButtonHeightConstant:CGFloat = 50.0
    static var themeCTAButtonCornerRadius:CGFloat = 22.0
    
    static func getThemeCTAButton(title: String) -> UIButton {
        let button = UIButton(type: .roundedRect)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(title, for: .normal)
        button.backgroundColor = .themeCTAButtonBackgroundColor
        button.tintColor = .white
        button.layer.cornerRadius = themeCTAButtonCornerRadius
        return button
    }
}
