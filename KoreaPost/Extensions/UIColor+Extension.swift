//
//  UIColor+Extension.swift
//  KoreaPost
//
//  Created by Akshay Phadke on 16/11/21.
//

import Foundation
import UIKit

extension UIColor {
    static var themeScreenBackgroundColor: UIColor {
        return UIColor(red: 0.898, green: 0.898, blue: 0.898, alpha: 1)
    }
    
    static var themeCTAButtonBackgroundColor: UIColor {
        //return UIColor(red: 221.0/225.0, green: 105.0/225.0, blue: 93.0/225.0, alpha: 1)
        return UIColor(red: 0.867, green: 0.412, blue: 0.365, alpha: 1)
    }
}
